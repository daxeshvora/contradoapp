﻿using ContradoApp.DAL.EntityModel;
using ContradoApp.DAL.Repository.Common;
using ContradoApp.DAL.Repository.Interface;

namespace ContradoApp.DAL.Repository.Implementation
{
    public class ProductAttributeLookupRepository : Repository<ProductAttributeLookup>, IProductAttributeLookupRepository
    {
        public ProductAttributeLookupRepository(ECommerceDemoContext _eCommerceDemoContext) : base(_eCommerceDemoContext)
        {
        }
    }
}
