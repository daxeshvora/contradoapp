﻿using ContradoApp.DAL.EntityModel;
using ContradoApp.DAL.Repository.Common;
using ContradoApp.DAL.Repository.Interface;

namespace ContradoApp.DAL.Repository.Implementation
{
    public class ProductAttributeRepository : Repository<ProductAttribute>, IProductAttributeRepository
    {
        public ProductAttributeRepository(ECommerceDemoContext _eCommerceDemoContext) : base(_eCommerceDemoContext)
        {
        }
    }
}
