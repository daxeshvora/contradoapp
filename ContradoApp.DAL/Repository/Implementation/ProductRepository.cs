﻿using ContradoApp.DAL.EntityModel;
using ContradoApp.DAL.Repository.Common;
using ContradoApp.DAL.Repository.Interface;

namespace ContradoApp.DAL.Repository.Implementation
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(ECommerceDemoContext _eCommerceDemoContext) : base(_eCommerceDemoContext)
        {
        }
    }
}
