﻿using ContradoApp.DAL.EntityModel;
using ContradoApp.DAL.Repository.Common;
using ContradoApp.DAL.Repository.Interface;

namespace ContradoApp.DAL.Repository.Implementation
{
    public class ProductCategoryRepository : Repository<ProductCategory>, IProductCategoryRepository
    {
        public ProductCategoryRepository(ECommerceDemoContext _eCommerceDemoContext) : base(_eCommerceDemoContext)
        {
        }
    }
}
