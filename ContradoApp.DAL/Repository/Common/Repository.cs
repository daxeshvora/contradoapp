﻿using ContradoApp.DAL.EntityModel.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ContradoApp.DAL.Repository.Common
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly ECommerceDemoContext eCommerceDemoContext;
        private readonly DbSet<T> _entities;

        public Repository(ECommerceDemoContext _eCommerceDemoContext)
        {
            eCommerceDemoContext = _eCommerceDemoContext;
            _entities = eCommerceDemoContext.Set<T>();
        }

        public async Task Insert(T entity)
        {
            await _entities.AddAsync(entity);
        }

        public async Task InsertRange(IEnumerable<T> entity)
        {
            await _entities.AddRangeAsync(entity);
        }

        public void Update(T entity)
        {
            eCommerceDemoContext.Attach(entity);
            eCommerceDemoContext.Entry(entity).State = EntityState.Modified;
        }

        public async Task Delete(int id)
        {
            T entity = await _entities.FindAsync(id);
            if (entity != null)
                _entities.Remove(entity);
            else
                throw new ArgumentNullException("entity");
        }

        public async Task Delete(long id)
        {
            T entity = await _entities.FindAsync(id);
            if (entity != null)
                _entities.Remove(entity);
            else
                throw new ArgumentNullException("entity");
        }

        public void DeleteRange(Expression<Func<T, bool>> predicate)
        {
            _entities.RemoveRange(_entities.Where(predicate));
        }

        public async Task<T> GetById(int id)
        {
            return await _entities.FindAsync(id);
        }

        public async Task<T> GetById(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] navigationProperties)
        {
            return (await GetByAsync(filterExpression: predicate, navigationProperties: navigationProperties)).FirstOrDefault();
        }

        public async Task<IEnumerable<T>> FindBy(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] navigationProperties)
        {
            return await GetByAsync(filterExpression: predicate, navigationProperties: navigationProperties);
        }

        public async Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] navigationProperties)
        {
            return await GetByAsync(navigationProperties: navigationProperties);
        }

        private async Task<IEnumerable<T>> GetByAsync(Expression<Func<T, bool>> filterExpression = null,
            int? pageNumber = null, int? recordsPerPage = null,
            Expression<Func<T, object>> orderByExpression = null, bool isDescending = false,
            params Expression<Func<T, object>>[] navigationProperties)
        {

            var querySharedContext = filterExpression != null ? _entities.Where(filterExpression) : eCommerceDemoContext.Set<T>();
            querySharedContext = ApplyPagingOrderingAndNavigation(querySharedContext,
                pageNumber, recordsPerPage, orderByExpression, isDescending, navigationProperties);
            return await querySharedContext.ToListAsync();
        }

        private static IQueryable<T> ApplyPagingOrderingAndNavigation(IQueryable<T> dbQuery,
           int? pageNumber = null, int? recordsPerPage = null,
           Expression<Func<T, object>> orderByExpression = null, bool isDescending = false,
           params Expression<Func<T, object>>[] navigationProperties)
        {
            if (navigationProperties != null)
            {
                foreach (var navigationProperty in navigationProperties)
                {
                    dbQuery = dbQuery.Include(navigationProperty);
                }
            }

            if (orderByExpression != null)
            {
                dbQuery = isDescending ? dbQuery.OrderByDescending(orderByExpression) : dbQuery.OrderBy(orderByExpression);
            }

            if (pageNumber.HasValue && recordsPerPage.HasValue)
            {
                dbQuery = dbQuery
                    .Skip(recordsPerPage.Value * (pageNumber.Value - 1))
                    .Take(recordsPerPage.Value);

            }

            return dbQuery;
        }

        
    }
}
