﻿using ContradoApp.DAL.EntityModel.Base;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ContradoApp.DAL.Repository.Common
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task Insert(T entity);
        Task InsertRange(IEnumerable<T> entity);
        void Update(T entity);
        Task Delete(int id);
        Task Delete(long id);
        void DeleteRange(Expression<Func<T, bool>> predicate);
        Task<T> GetById(int id);
        Task<T> GetById(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] navigationProperties);
        Task<IEnumerable<T>> FindBy(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] navigationProperties);
        Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] navigationProperties);


    }
}
