﻿using ContradoApp.DAL.EntityModel;
using ContradoApp.DAL.Repository.Common;

namespace ContradoApp.DAL.Repository.Interface
{
    public interface IProductRepository : IRepository<Product>
    {
    }
}
