﻿#nullable disable

using ContradoApp.DAL.EntityModel.Base;

namespace ContradoApp.DAL.EntityModel
{
    public partial class ProductAttributeLookup : BaseEntity
    {
        public int AttributeId { get; set; }
        public int ProdCatId { get; set; }
        public string AttributeName { get; set; }

        public virtual ProductCategory ProdCat { get; set; }
    }
}
