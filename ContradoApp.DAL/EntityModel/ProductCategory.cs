﻿using ContradoApp.DAL.EntityModel.Base;
using System.Collections.Generic;

#nullable disable

namespace ContradoApp.DAL.EntityModel
{
    public partial class ProductCategory : BaseEntity
    {
        public ProductCategory()
        {
            ProductAttributeLookups = new HashSet<ProductAttributeLookup>();
            Products = new HashSet<Product>();
        }

        public int ProdCatId { get; set; }
        public string CategoryName { get; set; }

        public virtual ICollection<ProductAttributeLookup> ProductAttributeLookups { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
