﻿#nullable disable

using ContradoApp.DAL.EntityModel.Base;

namespace ContradoApp.DAL.EntityModel
{
    public partial class Product : BaseEntity
    {
        public long ProductId { get; set; }
        public int ProdCatId { get; set; }
        public string ProdName { get; set; }
        public string ProdDescription { get; set; }

        public virtual ProductCategory ProdCat { get; set; }
    }
}
