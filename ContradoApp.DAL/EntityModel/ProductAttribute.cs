﻿#nullable disable

using ContradoApp.DAL.EntityModel.Base;

namespace ContradoApp.DAL.EntityModel
{
    public partial class ProductAttribute : BaseEntity
    {
        public long ProductAttributeId { get; set; }
        public long ProductId { get; set; }
        public int AttributeId { get; set; }
        public string AttributeValue { get; set; }

        public virtual ProductAttributeLookup Attribute { get; set; }
        public virtual Product Product { get; set; }
    }
}
