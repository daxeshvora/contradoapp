﻿using ContradoApp.DAL.Repository.Implementation;
using ContradoApp.DAL.Repository.Interface;
using System;
using System.Threading.Tasks;

namespace ContradoApp.DAL.UOW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ECommerceDemoContext _ECommerceDemoContext;

        public UnitOfWork(ECommerceDemoContext eCommerceDemoContext)
        {
            _ECommerceDemoContext = eCommerceDemoContext;
        }

        public IProductCategoryRepository _ProductCategoryRepository;
        public IProductCategoryRepository ProductCategoryRepository
        {
            get
            {
                return _ProductCategoryRepository != null ? _ProductCategoryRepository : new ProductCategoryRepository(_ECommerceDemoContext);
            }
        }

        public IProductRepository _ProductRepository;
        public IProductRepository ProductRepository
        {
            get
            {
                return _ProductRepository != null ? _ProductRepository : new ProductRepository(_ECommerceDemoContext);
            }
        }

        public IProductAttributeRepository _ProductAttributeRepository;
        public IProductAttributeRepository ProductAttributeRepository
        {
            get
            {
                return _ProductAttributeRepository != null ? _ProductAttributeRepository : new ProductAttributeRepository(_ECommerceDemoContext);
            }
        }

        public IProductAttributeLookupRepository _ProductAttributeLookupRepository;
        public IProductAttributeLookupRepository ProductAttributeLookupRepository
        {
            get
            {
                return _ProductAttributeLookupRepository != null ? _ProductAttributeLookupRepository : new ProductAttributeLookupRepository(_ECommerceDemoContext);
            }
        }

        public async Task<int> Commit()
        {
            return await _ECommerceDemoContext.SaveChangesAsync();
        }

        public void Dispose()
        {
            _ECommerceDemoContext?.Dispose();
        }

        public void Rollback()
        {
            throw new NotImplementedException();
        }
    }
}
