﻿using ContradoApp.DAL.Repository.Interface;
using System;
using System.Threading.Tasks;

namespace ContradoApp.DAL.UOW
{
    public interface IUnitOfWork : IDisposable
    {
        Task<int> Commit();
        void Rollback();

        IProductCategoryRepository ProductCategoryRepository { get; }
        IProductRepository ProductRepository { get; }
        IProductAttributeRepository ProductAttributeRepository { get; }
        IProductAttributeLookupRepository ProductAttributeLookupRepository { get; }
    }
}
