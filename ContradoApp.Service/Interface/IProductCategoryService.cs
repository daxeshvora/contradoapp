﻿using ContradoApp.DAL.EntityModel;
using ContradoApp.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ContradoApp.Service.Interface
{
    public interface IProductCategoryService
    {
        Task<IEnumerable<ProductCategoryViewModel>> GetAll();
        Task<ProductCategory> Get(int id);
        Task<ProductCategory> GetWithDetails(int id);
    }
}
