﻿using ContradoApp.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ContradoApp.Service.Interface
{
    public interface IProductService
    {
        Task<IEnumerable<ProductViewModel>> GetAll();
        Task<IEnumerable<ProductAttributeLookupViewModel>> GetAttributeByCategoryId(int categoryId);
        Task<ProductViewModel> Get(int productId);
        Task Save(ProductViewModel product);
        Task DeleteProductAttribute(long productAttributeId);
    }
}
