﻿using ContradoApp.DAL.UOW;

namespace ContradoApp.Service.Base
{
    public class BaseService : IBaseService
    {
        public readonly IUnitOfWork Uow;
        public BaseService(IUnitOfWork unitOfWork)
        {
            Uow = unitOfWork;
        }
    }
}
