﻿using AutoMapper;
using ContradoApp.DAL.EntityModel;
using ContradoApp.ViewModel;

namespace ContradoApp.Service.Mapper
{
    public class ProductMapper : Profile
    {
        public ProductMapper()
        {
            CreateMap<Product, ProductViewModel>()
                .ForMember(dest => dest.ProdCatName,
                    opt => opt.MapFrom(src => src.ProdCat != null ? src.ProdCat.CategoryName : ""))
                .ReverseMap();

            CreateMap<ProductCategory, ProductCategoryViewModel>()
                .ReverseMap();

            CreateMap<ProductAttributeLookup, ProductAttributeLookupViewModel>()
                .ReverseMap();

            CreateMap<ProductAttribute, ProductAttributeViewModel>()
                .ReverseMap();
        }
    }
}
