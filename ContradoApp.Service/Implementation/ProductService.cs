﻿using AutoMapper;
using ContradoApp.DAL.EntityModel;
using ContradoApp.DAL.UOW;
using ContradoApp.Service.Base;
using ContradoApp.Service.Interface;
using ContradoApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ContradoApp.Service.Implementation
{
    public class ProductService : BaseService, IProductService
    {
        private readonly IMapper _mapper;

        public ProductService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork)
        {
            _mapper = mapper;
        }

        private readonly Expression<Func<Product, object>>[] _defaultNavigationProperties =
        {
            a => a.ProdCat,
        };

        public async Task<IEnumerable<ProductViewModel>> GetAll()
        {
            var entities = await Uow.ProductRepository.GetAllAsync(_defaultNavigationProperties);
            return entities.Select(entity => _mapper.Map<ProductViewModel>(entity));
        }

        public async Task<IEnumerable<ProductAttributeLookupViewModel>> GetAttributeByCategoryId(int categoryId)
        {
            var entities = await Uow.ProductAttributeLookupRepository.FindBy(o => o.ProdCatId == categoryId);
            return entities.Select(entity => _mapper.Map<ProductAttributeLookupViewModel>(entity));
        }

        public async Task<ProductViewModel> Get(int productId)
        {
            var entity = await Uow.ProductRepository.GetById(o => o.ProductId == productId, _defaultNavigationProperties);
            if (entity == null)
                return null;
            var dto = _mapper.Map<ProductViewModel>(entity);
            var productMappedAttribute = await Uow.ProductAttributeRepository.FindBy(o => o.ProductId == productId);
            dto.MappedAttribute = productMappedAttribute?.Select(eachAttr => _mapper.Map<ProductAttributeViewModel>(eachAttr)).ToList();
            return dto;
        }

        public async Task Save(ProductViewModel product)
        {
            var entity = _mapper.Map<Product>(product);
            if (product.ProductId > 0)
            {
                Uow.ProductRepository.Update(entity);
                var attributeRecordToUpdate = product.MappedAttribute?.Where(o => o.ProductAttributeId > 0);
                if (attributeRecordToUpdate?.Any() ?? false)
                {
                    var mappedAttributeEntityToUpdate = _mapper.Map<IEnumerable<ProductAttribute>>(attributeRecordToUpdate);
                    foreach (var eachProperty in mappedAttributeEntityToUpdate)
                    {
                        Uow.ProductAttributeRepository.Update(eachProperty);
                    }
                }
            }
            else
            {
                await Uow.ProductRepository.Insert(entity);
            }

            var attributeRecordToAdd = product.MappedAttribute?.Where(o => o.ProductAttributeId == 0);
            if (attributeRecordToAdd?.Any() ?? false)
            {
                var mappedAttributeEntity = _mapper.Map<IEnumerable<ProductAttribute>>(attributeRecordToAdd);
                foreach (var eachProperty in mappedAttributeEntity)
                {
                    eachProperty.Product = entity;
                }
                await Uow.ProductAttributeRepository.InsertRange(mappedAttributeEntity);
            }
            
            await Uow.Commit();
        }

        public async Task DeleteProductAttribute(long productAttributeId)
        {
            await Uow.ProductAttributeRepository.Delete(productAttributeId);
            await Uow.Commit();
        }
    }
}
