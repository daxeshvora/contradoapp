﻿using AutoMapper;
using ContradoApp.DAL.EntityModel;
using ContradoApp.DAL.UOW;
using ContradoApp.Service.Base;
using ContradoApp.Service.Interface;
using ContradoApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ContradoApp.Service.Implementation
{
    public class ProductCategoryService : BaseService, IProductCategoryService
    {
        private readonly IMapper _mapper;

        public ProductCategoryService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork)
        {
            _mapper = mapper;
        }

        public async Task<ProductCategory> Get(int id)
        {
            return await Uow.ProductCategoryRepository.GetById(id);
        }

        private readonly Expression<Func<ProductCategory, object>>[] _defaultNavigationProperties =
        {
            a => a.ProductAttributeLookups,
        };

        public async Task<ProductCategory> GetWithDetails(int id)
        {
            return await Uow.ProductCategoryRepository.GetById(o => o.ProdCatId == id, _defaultNavigationProperties);
        }

        public async Task<IEnumerable<ProductCategoryViewModel>> GetAll()
        {
            var entities = await Uow.ProductCategoryRepository.GetAllAsync();
            return entities.Select(entity => _mapper.Map<ProductCategoryViewModel>(entity));
        }
    }
}
