﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ContradoApp.ViewModel
{
    public class ProductAttributeViewModel
    {
        public long ProductAttributeId { get; set; }
        public long ProductId { get; set; }
        [Required]
        public int AttributeId { get; set; }
        [Required]
        public string AttributeValue { get; set; }
        public int Index { get; set; }
        public List<ProductAttributeLookupViewModel> AttributeSelectionOption { get; set; }
    }
}
