﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ContradoApp.ViewModel
{
    public class CategorySelectionViewModel
    {
        public List<ProductCategoryViewModel> CategoryData { get; set; }

        [Required]
        public int SelectedCategory { get; set; }
    }
}
