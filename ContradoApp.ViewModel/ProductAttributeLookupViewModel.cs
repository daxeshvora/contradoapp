﻿
namespace ContradoApp.ViewModel
{
    public class ProductAttributeLookupViewModel
    {
        public int AttributeId { get; set; }
        public int ProdCatId { get; set; }
        public string AttributeName { get; set; }
    }
}
