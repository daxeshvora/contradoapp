﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ContradoApp.ViewModel
{
    public class ProductViewModel
    {
        public long ProductId { get; set; }
        public int ProdCatId { get; set; }
        [Required]
        public string ProdName { get; set; }
        [MaxLength(250)]
        public string ProdDescription { get; set; }
        public string ProdCatName { get; set; }

        public List<ProductAttributeLookupViewModel> AttributeAsPerCategory { get; set; }

        public List<ProductAttributeViewModel> MappedAttribute { get; set; }
    }
}
