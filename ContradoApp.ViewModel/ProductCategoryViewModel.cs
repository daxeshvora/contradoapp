﻿namespace ContradoApp.ViewModel
{
    public class ProductCategoryViewModel
    {
        public int ProdCatId { get; set; }
        public string CategoryName { get; set; }
    }
}
