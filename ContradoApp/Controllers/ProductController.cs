﻿using ContradoApp.Models;
using ContradoApp.Service.Interface;
using ContradoApp.ViewModel;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace ContradoApp.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductService _productService;
        private readonly IProductCategoryService _productCategoryService;

        public ProductController(IProductService productService, IProductCategoryService productCategoryService)
        {
            _productService = productService;
            _productCategoryService = productCategoryService;
        }

        public async Task<IActionResult> Index()
        {
            var prodList = await _productService.GetAll();
            return View(prodList);
        }

        public async Task<IActionResult> CategorySelection()
        {
            var prodCategoryList = await _productCategoryService.GetAll();
            var categorySelection = new CategorySelectionViewModel()
            {
                CategoryData = prodCategoryList.ToList()
            };
            return View(categorySelection);
        }

        [HttpPost]
        public async Task<IActionResult> CategorySelection(int SelectedCategory)
        {
            if (SelectedCategory > 0)
            {
                var categoryData = await _productCategoryService.Get(SelectedCategory);
                if (categoryData != null)
                {
                    return RedirectToAction("CreateOrUpdate", new { categoryId = SelectedCategory });
                }
            }
            return RedirectToAction("CategorySelection");
        }

        public async Task<IActionResult> CreateOrUpdate(int categoryId = 0, int productId = 0)
        {
            var productViewModel = new ProductViewModel();
            if (productId > 0)
            {
                // Edit Product
                productViewModel = await _productService.Get(productId);
                if (productViewModel == null)
                {
                    return RedirectToAction("Index");
                }
            }
            else if (categoryId > 0)
            {
                // Add Product
                var categoryDetails = await _productCategoryService.Get(categoryId);
                if (categoryDetails != null)
                {
                    productViewModel.ProdCatId = categoryId;
                    productViewModel.ProdCatName = categoryDetails.CategoryName;
                }
            }

            if (productViewModel.ProdCatId > 0)
            {
                productViewModel.AttributeAsPerCategory = (await _productService.GetAttributeByCategoryId(productViewModel.ProdCatId))?.ToList();
                return View(productViewModel);
            }

            return RedirectToAction("CategorySelection");
        }

        [HttpPost]
        public async Task<IActionResult> CreateOrUpdate(ProductViewModel productViewModel)
        {
            if (ModelState.IsValid)
            {
                await _productService.Save(productViewModel);
                return RedirectToAction("Index");
            }
            else
            {
                TempData[MessageType.ErrorMessage.ToString()] = string.Join(" | ", ModelState.Values
                    .SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage));
                productViewModel.AttributeAsPerCategory = (await _productService.GetAttributeByCategoryId(productViewModel.ProdCatId))?.ToList();
            }
            return View(productViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> AddAttributeSelection(int categoryId, int index = 0)
        {
            if (categoryId > 0)
            {
                var productAttribute = new ProductAttributeViewModel()
                {
                    Index = index
                };
                productAttribute.AttributeSelectionOption = (await _productService.GetAttributeByCategoryId(categoryId))?.ToList();
                return PartialView("_ProductMappedAttribute", productAttribute);
            }
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> DeleteAttribute(long productAttributeId)
        {
            if (productAttributeId > 0)
            {
                await _productService.DeleteProductAttribute(productAttributeId);
            }
            return Ok();
        }
    }
}
