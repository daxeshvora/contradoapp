﻿var getMaxIndexTr = function () {
    var allIndex = [];
    $('tr[data-index]').each(function (i, el) {
        allIndex.push($(el).attr('data-index'));
    });
    return (Math.max.apply(Math, allIndex) + 1);
}

$(document).on('click',
    '.add-row',
    function () {
        var categoryId = $('#ProdCatId').val();
        addAttributeRow(categoryId);
    });

var addAttributeRow = function (categoryId) {
    var addURL = $('#addformurl').val();
    $.post(addURL,
        {
            index: getMaxIndexTr(),
            categoryId: categoryId
        },
        function () {
        }).done(function (data) {
            var newRowHtml = data;
            $('#AttributeList tbody').append(newRowHtml);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            alert("Error while adding row " + errorThrown);
        }).always(function () {

        });
}

var resetRowIndexAfterDelete = function (maxRowIndex, rowDeleteIndex) {
    $('tr[data-index="' + rowDeleteIndex + '"]').remove();
    for (var i = (parseInt(rowDeleteIndex) + 1); i <= parseInt(maxRowIndex); i++) {
        var currentTRToReplace = $('tr[data-index="' + i + '"]');
        var allInputs = $(currentTRToReplace).find('input,select');
        allInputs.each(function (curr_idx, curr_elem) {
            var og_name = $(curr_elem).attr('name');
            var current_name = og_name.replace('[' + i + ']', '[' + (i - 1) + ']');
            $(curr_elem).attr('name', current_name);
        });
        $(currentTRToReplace).attr("data-index", (i - 1));
    }
}

$(document).on('click',
    '.delete-row',
    function () {
        if (confirm("Are you sure you want to delete?")) {
            var idToDelete = $(this).attr('data-deleteId');
            var maxRowIndex = (getMaxIndexTr() - 1);
            var rowDeleteIndex = $(this).closest('tr').attr('data-index');
            if (idToDelete > 0) {
                var deleteURL = $('#deleteformurl').val();
                $.post(deleteURL,
                    {
                        productAttributeId: idToDelete
                    },
                    function () {
                    }).done(function (data) {
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        alert("Error while deleting attribute row " + errorThrown);
                    }).always(function () {

                    });
            }
            resetRowIndexAfterDelete(maxRowIndex, rowDeleteIndex);
        }
    });